package com.faeda.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.faeda.app.R;
import com.faeda.app.utils.Model;
import com.faeda.app.utils.RecyclerAdapter;
import com.faeda.app.utils.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Filter extends AppCompatActivity {
    private String TAG ="dashboard";
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private ArrayList<Model> list;
    private RecyclerAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount, PageCount=1, PageCountLock=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        initToolbar();

    }

    public void onClick(View v) {
        int id = v.getId();

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Tools.setSystemBarColor(this, R.color.colorPrimary);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                break;
            case R.id.action_profile:
                Intent intent2 = new Intent(this, Profile.class);
                this.startActivity(intent2);
                break;
            case R.id.action_login:
                Intent intent3 = new Intent(this, Login.class);
                this.startActivity(intent3);
                break;
            case R.id.action_register:
                Intent intent4 = new Intent(this, Register.class);
                this.startActivity(intent4);
                break;
            case R.id.action_need_help:
                Intent intent5 = new Intent(this, Login.class);
                this.startActivity(intent5);
                break;
             default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

}
