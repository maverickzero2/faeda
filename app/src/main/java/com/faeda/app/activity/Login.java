package com.faeda.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.faeda.app.R;
import com.faeda.app.utils.Tools;


public class Login extends AppCompatActivity {

    private View parent_view;
    private Button signIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        parent_view = findViewById(android.R.id.content);

        Tools.setSystemBarColor(this, android.R.color.white);
        Tools.setSystemBarLight(this);

        ((View) findViewById(R.id.sign_up_for_account)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(parent_view, "Sign up for an account", Snackbar.LENGTH_SHORT).show();
                Intent intent = new Intent(Login.this, Register.class);
                intent.putExtra("activity", "Register");
                startActivity(intent);

            }
        });

        signIn = findViewById(R.id.btn_sign_in);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Dashboard.class);
                intent.putExtra("activity", "Dashboard");
                startActivity(intent);
            }
        });
    }
}
